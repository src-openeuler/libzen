Name:           libzen
Version:        0.4.41
Release:        3
Summary:        Shared library for libmediainfo and medianfo*
Summary(ru):    Разделяемая библиотека для libmediainfo и medianfo*

License:        zlib
URL:            https://github.com/MediaArea/ZenLib
Source0:        https://mediaarea.net/download/source/%{name}/%{version}/%{name}_%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  doxygen
BuildRequires:  cmake
BuildRequires:  pkgconfig(zlib)

%description
Files shared library for libmediainfo and medianfo-*.

%description -l ru
Файлы разделяемой библиотеки для libmediainfo и medianfo-*.

%package        help
Summary:        Documentation for %{name}
Summary(ru):    Пакет с документацией для %{name}
Requires:       %{name} = %{version}-%{release}
BuildArch:      noarch

%description    help
Documentation files.

%description    help -l ru
Файлы документации %{name}.

%package        devel
Summary:        Include files and mandatory libraries for development
Summary(ru):    Пакет с файлами для разработки %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
Include files and mandatory libraries for development.

%description    devel -l ru
Файлы для разработки %{name}.

%prep
%autosetup -n ZenLib

#Correct documentation encoding and permissions
sed -i 's/.$//' *.txt
chmod 644 *.txt Source/Doc/Documentation.html

chmod 644 Source/ZenLib/*.h Source/ZenLib/*.cpp \
    Source/ZenLib/Format/Html/*.h Source/ZenLib/Format/Html/*.cpp \
    Source/ZenLib/Format/Http/*.h Source/ZenLib/Format/Http/*.cpp

%build
#Make documentation
pushd Source/Doc/
    doxygen -u Doxyfile
    doxygen Doxyfile
popd
cp Source/Doc/*.html ./

%cmake -S Project/CMake
%cmake_build

%install
%cmake_install

%files
%doc History.txt ReadMe.txt
%license License.txt
%{_libdir}/%{name}.so.*

%files help
%doc Documentation.html
%doc Doc

%files devel
%{_includedir}/ZenLib
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/cmake/zenlib/


%changelog
* Thu Dec 12 2024 Funda Wang <fundawang@yeah.net> - 0.4.41-3
- adopt to new cmake macro

* Wed Nov 20 2024 Funda Wang <fundawang@yeah.net> - 0.4.41-2
- adopt to new cmake macro

* Fri Aug 4 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 0.4.41-1
- update to 0.4.41

* Tue Feb 21 2023 liweiganga <liweiganga@uniontech.com> - 0.4.40-1
- upstream to 0.4.40

* Mon Nov 21 2022 liweiganga <liweiganga@uniontech.com> - 0.4.39-1
- update to 0.4.39

* Fri Aug 7 2020 weidong <weidong@uniontech.com> - 0.4.38-1
- Initial release for OpenEuler
